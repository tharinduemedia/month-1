package au.com.elegantmedia.extensionstest.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import au.com.elegantmedia.extensionstest.R
import au.com.elegantmedia.extensionstest.extensions.*
import au.com.elegantmedia.extensionstest.utils.Constant
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

/**
 * Created by Tharindu Jayakody on 30-07-2021
 * Elegant Media
 * tharindu.emedia@gmail.com
 */
class MainActivity : AppCompatActivity() {

    private val sharedPreferences by inject<SharedPreferences>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        button_save.setOnClickListener {
            if (isValidForm()) {
                val gender = if (rb_male.isChecked) {
                    Constant.MALE
                } else {
                    Constant.FEMALE
                }

                sharedPreferences.setValue(Constant.FULL_NAME, edit_full_name.getStringTrim())
                sharedPreferences.setValue(Constant.EMAIL, edit_email.getStringTrim())
                sharedPreferences.setValue(Constant.PHONE_NUMBER, edit_phone.getStringTrim())
                sharedPreferences.setValue(Constant.GENDER, gender)
                sharedPreferences.setValue(Constant.ADDRESS, edit_address.getStringTrim())

                Toast.makeText(this, getString(R.string.user_details_saved), Toast.LENGTH_SHORT)
                    .show()

                clearFields()
            }
        }

        button_view_details.setOnClickListener {
            if (sharedPreferences.contains(Constant.FULL_NAME) && sharedPreferences.contains(
                    Constant.EMAIL
                )
                && sharedPreferences.contains(Constant.PHONE_NUMBER) && sharedPreferences.contains(
                    Constant.GENDER
                ) && sharedPreferences.contains(Constant.ADDRESS)
            ) {
                startActivity(Intent(this@MainActivity, ViewDetailsActivity::class.java))
            } else {
                Toast.makeText(this, getString(R.string.user_not_found), Toast.LENGTH_SHORT).show()
            }
        }
    }

    //clear the all input fields
    private fun clearFields() {
        edit_full_name.setText("")
        edit_email.setText("")
        edit_phone.setText("")
        edit_address.setText("")
        rb_male.isChecked = true
    }


    //validate user inputs
    private fun isValidForm(): Boolean {
        val isFullName = edit_full_name.validateWithTextWatcher(
            til_name,
            getString(R.string.error_name_invalid)
        ) { s -> s.isNotEmpty() }

        val isEmail = edit_email.validateWithTextWatcher(
            til_email,
            getString(R.string.error_email_invalid)
        ) { s -> s.isValidEmail() }

        val isPhone = edit_phone.validateWithTextWatcher(
            til_phone,
            getString(R.string.error_phone_number_invalid)
        ) { s -> s.isValidPhone() }

        val isAddress = edit_address.validateWithTextWatcher(
            til_address,
            getString(R.string.error_address_invalid)
        ) { s -> s.isNotEmpty() }

        return isFullName && isEmail && isPhone && isAddress
    }
}