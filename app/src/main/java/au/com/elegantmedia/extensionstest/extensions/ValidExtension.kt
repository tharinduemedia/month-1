package au.com.elegantmedia.extensionstest.extensions

import android.util.Patterns

/**
 * validate email
 */
fun String.isValidEmail(): Boolean = this.isNotEmpty() &&
        Patterns.EMAIL_ADDRESS.matcher(this.trim()).matches()

/**
 * validate mobile
 */
fun String.isValidPhone(): Boolean = this.isNotEmpty() &&
        Patterns.PHONE.matcher(this.trim()).matches()
