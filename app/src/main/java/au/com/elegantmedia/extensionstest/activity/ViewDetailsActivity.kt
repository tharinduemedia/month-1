package au.com.elegantmedia.extensionstest.activity

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import au.com.elegantmedia.extensionstest.R
import au.com.elegantmedia.extensionstest.extensions.get
import au.com.elegantmedia.extensionstest.utils.Constant
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

/**
 * Created by Tharindu Jayakody on 30-07-2021
 * Elegant Media
 * tharindu.emedia@gmail.com
 */
class ViewDetailsActivity : AppCompatActivity() {

    private val sharedPreferences by inject<SharedPreferences>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_data)

        //display user data
        text_full_name.text = "Full Name : ${sharedPreferences.get<String>(Constant.FULL_NAME)}"
        text_email.text = "Email : ${sharedPreferences.get<String>(Constant.EMAIL)}"
        text_phone_number.text =
            "Phone Number : ${sharedPreferences.get<String>(Constant.PHONE_NUMBER)}"
        text_gender.text = "Gender : ${sharedPreferences.get<String>(Constant.GENDER)}"
        text_address.text = "Address : ${sharedPreferences.get<String>(Constant.ADDRESS)}"

    }
}