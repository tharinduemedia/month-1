package au.com.elegantmedia.extensionstest

import android.app.Application
import au.com.elegantmedia.extensionstest.utils.sharedPreferencesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Tharindu Jayakody on 30-07-2021
 * Elegant Media
 * tharindu.emedia@gmail.com
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MyApplication)

            modules(
                listOf(
                    sharedPreferencesModule
                )
            )
        }
    }
}