package au.com.elegantmedia.extensionstest.extensions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.core.content.ContextCompat
import au.com.elegantmedia.extensionstest.R
import com.google.android.material.textfield.TextInputLayout

/**
 * This extension supports EditText validation such as emails, maximum and minimum length
 * inspired from
 * @see https://proandroiddev.com/easy-edittext-content-validation-with-kotlin-316d835d25b3
 * */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            afterTextChanged.invoke(s.toString().trim())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}

/**
 * Validator for EditTextLayout
 */
fun EditText.validateWithTextWatcher(
    mTextInputLayout: TextInputLayout,
    message: String,
    validator: (String) -> Boolean
): Boolean {
    this.afterTextChanged {
        mTextInputLayout.error = if (validator(it)) {
            this.setTextColor(
                ContextCompat.getColor(
                    this.context,
                    R.color.colorBlack
                )
            )
            null
        } else {
            this.setTextColor(
                ContextCompat.getColor(
                    this.context,
                    R.color.colorBlack
                )
            )
            message
        }
        this.background.clearColorFilter()
    }
    mTextInputLayout.error = if (validator(this.getStringTrim())) {
        this.setTextColor(
            ContextCompat.getColor(
                this.context,
                R.color.colorBlack
            )
        )
        null
    } else {
        this.setTextColor(
            ContextCompat.getColor(
                this.context,
                R.color.colorRed
            )
        )
        message

    }
    this.background.clearColorFilter()

    return validator(this.getStringTrim())
}

/**
 * @return String value of the EditTextView
 * */
fun EditText.getString(): String {
    return this.text.toString()
}

/**
 * @return Trimmed String value of the EditTextView
 * */
fun EditText.getStringTrim(): String {
    return this.getString().trim()
}

