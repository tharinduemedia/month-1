package au.com.elegantmedia.extensionstest.utils

/**
 * Created by Tharindu Jayakody on 30-07-2021
 * Elegant Media
 * tharindu.emedia@gmail.com
 */
object Constant {

    const val FULL_NAME: String = "full_name"
    const val EMAIL: String = "email"
    const val PHONE_NUMBER: String = "phone_number"
    const val GENDER: String = "gender"
    const val ADDRESS: String = "address"
    const val PREF_NAME = "extensions_test"

    const val MALE = "Male"
    const val FEMALE = "Female"
}
